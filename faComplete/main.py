import os
import time, sys, math, json, datetime

import argparse
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import WebDriverException, NoSuchElementException, TimeoutException
from termcolor import colored
import requests
import pprint
from tqdm import tqdm
import numpy


class faComplete():

    faUsername = None
    faPassword = None
    driver = None
    debug = True
    chromedriver_path = "chromedriver.exe"
    opts = webdriver.ChromeOptions()
    headless = False
    session = None
    apiUrl = None
    apiKey = None
    pageSize = 20,
    debugRequests = False
    delayTime = None
    completeJobs = True
    windowsOS = True


    def __init__(self):
        pass


    ######################
    #      Web Work      #
    ######################
    def login(self):
        self.driver.get('https://auth.fieldaware.com/')
        search_box = self.driver.find_element_by_id('email')
        search_box.send_keys(self.faUsername)
        search_box = self.driver.find_element_by_id('password')
        search_box.send_keys(self.faPassword)
        search_box.submit()
        time.sleep(5)  # give the page some time to load

        print(colored("Waiting for login",'magenta'))
        try:
            WebDriverWait(self.driver, 20).until(
                EC.presence_of_element_located((By.XPATH, "//a[@href='/account/jobs_and_quotes']"))
            )
        except NoSuchElementException as e:
            print(e)
            print(colored("Login failed", 'red'))
            return False
        except TimeoutException as t:
            print(t)
            print(colored("Login failed. Check your credentials", 'red'))
            return False

        print(colored("Logged in", 'magenta'))

        return True

    def doJob(self, jobId):
        searchBox = self.driver.find_element_by_xpath("//input[@class='fa-QuickSearch-input']")
        searchBox.clear()
        searchBox.send_keys(jobId)
        goBtn = self.driver.find_element_by_xpath("//button[@class='fa-QuickSearch-go']")
        goBtn.click()
        try:
            WebDriverWait(self.driver, 15).until(
                EC.presence_of_element_located((By.XPATH, "//span[contains(text(),'{}')]".format(str(jobId).upper())))
            )
        except TimeoutException as t:
            print(t)
            print(colored("Failed to find the Job ID ({}) on the page".format(jobId), 'red'))
            return False

        if self.debug:
            print(colored("Found job page", 'green'))

        if self.startJob():
            self.saveJob()
            time.sleep(self.delayTime)
            if self.completeJobs:
                WebDriverWait(self.driver, 15).until(
                    EC.presence_of_element_located((By.XPATH, "//span[contains(text(),'{}')]".format(str(jobId).upper())))
                )
                self.completeJob()
                self.saveJob()
                time.sleep(self.delayTime)
            return True
        else:
            if self.completeJobs:
                if self.completeJob():
                    self.saveJob()
                    time.sleep(self.delayTime)
                    return True
                else:
                    if self.debug:
                        print(colored('Job has already been completed. Returning true', 'red'))
                    return True
            else:
                return True

    def isStarted(self):
        WebDriverWait(self.driver, 15).until(
            EC.presence_of_element_located((By.XPATH, "//span[@id='control_start']"))
        )
        # print(colored("Found Control Complete", 'green'))
        time.sleep(self.delayTime)
        completeBtn = self.driver.find_element_by_xpath("//span[@id='control_start']")

        if completeBtn.get_attribute('class').find('disabled') == -1:
            return False
        else:
            return True

    def startJob(self):
        WebDriverWait(self.driver, 15).until(
            EC.presence_of_element_located((By.XPATH, "//div[@id='sidebar']/div/a"))
        )
        time.sleep(self.delayTime)
        startBtn = self.driver.find_element_by_xpath("//span[@id='control_start']")
        if startBtn.get_attribute('class').find('disabled') == -1:
            try:
                startBtn.click()
            except WebDriverException as e:
                if 'Element is not clickable at point' in e.msg:
                    self.driver.execute_script(
                        'arguments[0].click()',
                        startBtn
                    )
                else:
                    raise
            if self.debug:
                print(colored("Clicked Start", 'green'))
            return True
        else:
            if self.debug:
                print(colored('Job already started', 'yellow'))
            return False

    def isCompleted(self):
        WebDriverWait(self.driver, 15).until(
            EC.presence_of_element_located((By.XPATH, "//span[@id='control_complete']"))
        )
        time.sleep(self.delayTime)
        completeBtn = self.driver.find_element_by_xpath("//span[@id='control_complete']")

        if completeBtn.get_attribute('class').find('disabled') == -1:
            return False
        else:
            return True

    def completeJob(self):
        WebDriverWait(self.driver, 15).until(
            EC.presence_of_element_located((By.XPATH, "//div[@id='sidebar']/div/a"))
        )
        WebDriverWait(self.driver, 15).until(
            EC.presence_of_element_located((By.XPATH, "//span[@id='control_complete']"))
        )
        # time.sleep(self.delayTime)
        completeBtn = self.driver.find_element_by_xpath("//span[@id='control_complete']")
        if completeBtn.get_attribute('class').find('disabled') == -1:
            try:
                completeBtn.click()
            except WebDriverException as e:
                if 'Element is not clickable at point' in e.msg:
                    self.driver.execute_script(
                        'arguments[0].click()',
                        completeBtn
                    )
                else:
                    raise
            if self.debug:
                print(colored("Clicked Complete", 'green'))
            return True
        else:
            if self.debug:
                print(colored('Job already completed', 'yellow'))
            return False

    def isSaved(self):
        WebDriverWait(self.driver, 15).until(
            EC.presence_of_element_located((By.XPATH, "//div[@id='sidebar']/div/a"))
        )
        time.sleep(self.delayTime)
        saveBtn = self.driver.find_element_by_xpath("//div[@id='sidebar']/div/a")

        if saveBtn.get_attribute('class').find('enabled') == -1:
            return False
        else:
            return True

    def saveJob(self):
        WebDriverWait(self.driver, 15).until(
            EC.presence_of_element_located((By.XPATH, "//div[@id='sidebar']/div/a"))
        )
        # time.sleep(self.delayTime)
        saveBtn = self.driver.find_element_by_xpath("//div[@id='sidebar']/div/a")
        if saveBtn.get_attribute('class').find('enabled') >= 0:
            try:
                saveBtn.click()
            except WebDriverException as e:
                if 'Element is not clickable at point' in e.msg:
                    self.driver.execute_script(
                        'arguments[0].click()',
                        saveBtn
                    )
                else:
                    raise
            if self.debug:
                print(colored("Clicked Save", 'green'))
            return True
        else:
            if self.debug:
                print(colored('Job already saved', 'yellow'))
            return False


    ######################
    #      API Work      #
    ######################
    def getJobs(self, save = False):

        data = []
        dataStart = []
        dataComplete = []
        curPage = 0
        url = "{0}job/?start=1969-12-31T01%3A00&end=2069-01-01T17%3A00&pageSize={1}&page={2}&sortedBy=jobId"
        r = self.session.get(url.format(self.apiUrl, self.pageSize, curPage))
        rjs = r.json()
        if self.debugRequests:
            print(colored('Request Output Page 0:', 'magenta'))
            pprint.pprint(rjs)
        count = rjs['count']
        pages = int(math.ceil(count/self.pageSize))
        for item in rjs['items']:
            if item['state'][0].find('completed') == -1:
                if item['jobLead'] is not None:
                    data.append(item['jobId'])
                    if item['state'][0] == 'scheduled':
                        dataStart.append(item['jobId'])
                        dataComplete.append(item['jobId'])
                    if item['state'][0] == 'active' or item['state'][0] == 'paused':
                        dataComplete.append(item['jobId'])


        pbar = tqdm(iterable=curPage, total=pages)
        while curPage < pages:
            curPage += 1
            r = self.session.get(url.format(self.apiUrl, self.pageSize, curPage))
            rjs = r.json()
            if self.debugRequests:
                print(colored('Request Output Page {}:'.format(curPage), 'magenta'))
                pprint.pprint(rjs)

            for item in rjs['items']:
                if item['state'][0].find('completed') == -1:
                    if item['jobLead'] is not None:
                        data.append(item['jobId'])
                        if item['state'][0] == 'scheduled':
                            dataStart.append(item['jobId'])
                            dataComplete.append(item['jobId'])
                        if item['state'][0] == 'active' or item['state'][0] == 'paused':
                            dataComplete.append(item['jobId'])
            pbar.update()

        if save:
            self.saveJson('jobsOnline.json', data)
            self.saveJson('jobsToStart.json', dataStart)
            self.saveJson('jobsToComplete.json', dataComplete)

        return data


    ######################
    #       Utils        #
    ######################
    def makeSettingsJson(self):
        j = {
        "apiKey": "",
        "fieldawareUsername": "",
        "fieldawarePassword": "",
        "apiUrl": "https://api.fieldaware.net/",
        "chromeHeadless": False,
        "apiPageSize": 20,
        "delayTime": 2,
        "debug": False,
        "debugRequests": False,
        "windows_os": True
        }
        print(colored("\n\nSettings.json has been created.\nPlease edit the file and fill in the missing info before continuing.\n\n", 'magenta'))
        self.saveJson('settings.json', j, True)

    def loadSettings(self):
        if not os.path.isfile('settings.json'):
            print(colored("\n\nError settings.json not found!\nPlease run \'{0} -s\' to create the settings file.\nOnce created edit the file and fill in the missing info.\nThen re-run {0} with the desired flags.\n\n".format(os.path.basename(__file__)), 'red'))
            return False
        with open('settings.json', 'r') as fp:
            data = json.load(fp)
            if data['debug']:
                print("Settings:")
                pprint.pprint(data)
        self.pageSize = data['apiPageSize']
        self.apiUrl = data['apiUrl']
        self.apiKey = data['apiKey']
        self.headless = data['chromeHeadless']
        self.faPassword = data['fieldawarePassword']
        self.faUsername = data['fieldawareUsername']
        self.delayTime = data['delayTime']
        self.debug = data['debug']
        self.debugRequests = data['debugRequests']
        self.windowsOS = data['windows_os']

    def closeDriver(self):
        self.driver.close()

    def startDriver(self):
        # os.chdir(os.path.dirname(__file__))

        if not self.windowsOS:
            self.chromedriver_path = "chromedriver"

        self.chromedriver_path = os.path.normpath(os.getcwd() + "/" + self.chromedriver_path)
        if self.debug:
            print(self.chromedriver_path)

        self.opts.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0")
        if self.headless:
            self.opts.add_argument('--headless')
        self.driver = webdriver.Chrome(chrome_options=self.opts, executable_path=self.chromedriver_path)
        if self.debug:
            print("Driver Url: {}".format(self.driver.command_executor._url))
            print("Driver Session: {}".format(self.driver.session_id))
            
    def setupSession(self):
        self.session = requests.Session()
        self.session.headers.update({'Authorization': 'Token {}'.format(self.apiKey)})
        self.session.headers.update({'Accept-Type': 'application/json'})
        if self.debug:
            print("Session Headers: {}".format(str(self.session.headers)))

    def saveJson(self, file, data, settings = False):
        if settings:
            dr = ''
        else:
            dr = 'data/'

        os.makedirs(os.path.dirname('{0}{1}'.format(dr, file)), exist_ok=True)

        with open('{0}{1}'.format(dr, file), 'wt') as out:
            json.dump(data, out, sort_keys=True, indent=4, separators=(',', ': '))

    def loadJson(self, file, settings = False):
        if settings:
            dr = ''
        else:
            dr = 'data/'
        if not os.path.isfile('{0}{1}'.format(dr, file)):
            return False
        with open('{0}{1}'.format(dr, file)) as infile:
            data = json.load(infile)
        return data

    def fileExists(self, file, settings = False):
        if settings:
            dr = ''
        else:
            dr = 'data/'
        if not os.path.isfile('{0}{1}'.format(dr, file)):
            return False

        return True

    def compareJobsJson(self):
        jobs = self.loadJson('jobs.json')
        jobsStart = self.loadJson('jobsToStart.json')
        jobsComplete = self.loadJson('jobsToComplete.json')
        jobsOnline = self.loadJson('jobsOnline.json')

        m = list(set(jobsOnline).intersection(jobs))
        m.sort()
        if self.debug:
            print('jobsGenerated.json')
            pprint.pprint(m)
        self.saveJson('jobsGenerated.json', m)

        s = list(set(jobsStart).intersection(jobs))
        s.sort()
        if self.debug:
            print('jobsToStart.json')
            pprint.pprint(s)
        self.saveJson('jobsToStart.json', s)

        c = list(set(jobsComplete).intersection(jobs))
        c.sort()
        if self.debug:
            print('jobsToComplete.json')
            pprint.pprint(c)
        self.saveJson('jobsToComplete.json', c)

    # def split(self, a, n):
    #     k, m = divmod(len(a), n)
    #     return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))

    def split(self, f, n):
        data = self.loadJson(f)
        if data is False:
            raise FileNotFoundError('{0} was not found. Exiting'.format(f))

        return numpy.array_split(data, n, 0)


######################
#        MAIN        #
######################
try:
    assert sys.version_info >= (3,3)
except AssertionError:
    print(colored('Python 3.3 or newer required to run this script!', 'red'))
    exit(1)

parser = argparse.ArgumentParser(description='Select only one of the modes below.')
modes = parser.add_argument_group('Modes')
modes.add_argument('-s', '--setup', help='Create settings json file', action='store_true')
modes.add_argument('-j', '--jobs', help='Generate a json list of jobs that are not completed. If Data\jobs.json exists (from faApi project) it will be used to update the generated list.', action='store_true')
modes.add_argument('-x', '--execute', help='Complete the jobs listed in the jobsOnline.json file', action='store_true')
modes.add_argument('-p', '--split', help='Split the jobsGenerated, jobsToComplete.json, and jobsToStart.json into X files (e.g. -p 2)', type=int, action='store')

if not len(sys.argv) > 1:
    parser.print_help()
    exit(1)

args = parser.parse_args()

c = 0
for i in vars(args):
    if getattr(args, i):
        c += 1
if c > 1:
    parser.print_help()
    print()
    print(colored('Too may modes selected. Select only one!', 'red'))
    exit(1)

fac = faComplete()

if args.setup:
    fac.makeSettingsJson()
    exit(0)


if args.jobs:
    jobsBegin = datetime.datetime.now()
    print(colored('Process Begin: {0}'.format(datetime.datetime.now().isoformat()), 'magenta'))
    print(colored('Loaded setttings.json', 'magenta'))
    fac.loadSettings()
    print(colored('Setup api session', 'magenta'))
    fac.setupSession()
    print(colored('Iterating api for jobs', 'magenta'))
    data = fac.getJobs(True)
    print(colored('Created data/jobsOnline.json', 'magenta'))
    if fac.loadJson('jobs.json') is not False:
        fac.compareJobsJson()
    if fac.debug:
        pprint.pprint(data)
    print(colored('Process End: {0}'.format(datetime.datetime.now().isoformat()), 'magenta'))
    print(colored('Process Elapsed Time: {0}'.format(datetime.datetime.now() - jobsBegin), 'magenta'))
    exit(0)

if args.execute:
    executeBegin = datetime.datetime.now()
    print(colored('Process Begin: {0}'.format(executeBegin.isoformat()), 'magenta'))
    print(colored('Loaded setttings.json', 'magenta'))
    fac.loadSettings()
    dataStart = fac.loadJson('jobsToStart.json')
    dataComplete = fac.loadJson('jobsToComplete.json')

    print(colored('Starting Chromedriver', 'magenta'))
    fac.startDriver()
    print(colored('Logging into FieldAware', 'magenta'))
    if not fac.login():
        exit(1)
    print(colored('Starting Jobs', 'magenta'))
    fac.completeJobs = False
    with tqdm(total=len(dataStart)) as pbar:
        for job in dataStart[:]:
            if fac.doJob(job):
                dataStart.remove(job)
                fac.saveJson('jobsToStart.json', dataStart)
            pbar.update()
    print(colored('Completing Jobs', 'magenta'))
    fac.completeJobs = True
    with tqdm(total=len(dataComplete)) as pbar:
        for job in dataComplete[:]:
            if fac.doJob(job):
                dataComplete.remove(job)
                fac.saveJson('jobsToComplete.json', dataComplete)
            pbar.update()

    if len(dataComplete) > 0:
        print("The jobsToComplete.json contains the jobs that could not be completed")
        pprint.pprint(dataComplete)
    time.sleep(fac.delayTime)
    time.sleep(5)
    fac.closeDriver()
    print(colored('Process End: {0}'.format(datetime.datetime.now().isoformat()), 'magenta'))
    print(colored('Process Elapsed Time: {0}'.format(datetime.datetime.now() - executeBegin), 'magenta'))
    exit(0)

if args.split is not None:
    print(colored('Splitting json files into {0} pieces...'.format(args.split), 'magenta'))
    rtn = fac.split('jobsGenerated.json', args.split)
    x = 1
    for data in rtn:
        fac.saveJson('split_{0}/jobsGenerated.json'.format(x), list(data))
        print(colored('Created split_{0}/jobsGenerated.json'.format(x), 'magenta'))
        x = x + 1

    rtn = fac.split('jobsToStart.json', args.split)
    x = 1
    for data in rtn:
        fac.saveJson('split_{0}/jobsToStart.json'.format(x), list(data))
        print(colored('Created split_{0}/jobsToStart.json'.format(x), 'magenta'))
        x = x + 1

    rtn = fac.split('jobsToComplete.json', args.split)
    x = 1
    for data in rtn:
        fac.saveJson('split_{0}/jobsToComplete.json'.format(x), list(data))
        print(colored('Created split_{0}/jobsToComplete.json'.format(x), 'magenta'))
        x = x + 1
