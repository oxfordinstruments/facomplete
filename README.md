FieldAware Complete Jobs
========================
### Python script using Seleniuum WebDriver to control chromedriver for starting and completing jobs in FieldAware.

#### Please see the requirements.txt for the needed python modules.

----
**For the first run execute "main.py -s" to create the settings.json file needed for the script. Open your favorite text editor and fill in the missing values such as api key/token, username, and password.**

----

Executing "main.py" with no arguments will print the help text.

----
Executing "main.py -j" will connect to the api and retreive all of the jobs that are not completed and the job lead is set as a user. The jobsOnline.json file will be saved into the data folder.

----
Executing "main.py -x" will process all of the job numbers present in the jobsOnline.json file. As each job is marked as completed it will be removed from the respective jobsToStart.json or jobsToComplete.json file.

If a jobs.json file exists (from the faApi project) it will be used to generate an intersection of jobsOnline.json and jobsGenerated.json will be produced and used to create jobsToStart.json and jobsToComplete.json files.

_Note: If a job fails to be marked as completed or started it will remain in the respective json file, and you will be presented with output at the end of the script listing all the fails._

----

Executing "main.py -p X" will split the jobsGenerated, jobsToComplete.json, and jobsToStart.json into X files in data/split_X folders.

Example: main.py -p 2  will produce data/split_1/file.json and data/split_2/file.json

-------------------------------------------------------------------------------------------